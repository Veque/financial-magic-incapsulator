using FMI.Web.Services;
using NUnit.Framework;
using System;

namespace FMI.Tests
{
    public class Tests
    {
        FakeBondsService _store = new FakeBondsService();
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestProfitability()
        {
            var bond = _store.GetBondsAsync().Result[0];

            foreach(var income in bond.GetIncomeStream())
            {
                Console.WriteLine($"Income: {income.Value}, {income.When.ToShortDateString()}");
            }

            Console.WriteLine(bond.Profitability.ToString("N2"));
        }
    }
}