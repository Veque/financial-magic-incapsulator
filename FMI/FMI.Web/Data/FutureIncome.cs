﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMI.Web.Data
{
    public class FutureIncome
    {
        public FutureIncome(double value, DateTime when)
        {
            Value = value;
            When = when;
        }
        public double Value { get; set; }
        public DateTime When { get; set; }
    }
}
