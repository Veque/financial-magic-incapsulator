﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMI.Web.Data
{
    public class Bond
    {
        [BsonIgnore]
        public bool IsDeleted { get; set; }
        [BsonIgnore]
        public bool IsEdited { get; set; }
        [BsonIgnore]
        public bool IsNew { get; set; }

        public ObjectId _id { get; set; } 
        public Bond()
        {
            Name = "New Bond";
            FaceValue = 1000;
            Price = 1000;
            Issued = DateTime.Today;
            CouponsInYear = 2;
            CouponValue = 25;
            TotalCoupons = 20;
        }

        private const int DaysInYear = 365;

        [BsonIgnore]
        public Func<double> DiscountRate { get; set; } = () => 0.05;

        public string Name { get; set; }
        public double Price { get; set; }
        public double FaceValue { get; set; }
        public double CouponValue { get; set; }

        public string CouponString { get => $"{CouponValue.ToString("N2")} x {CouponsInYear.ToString("N0")}"; }

        public int CouponsInYear { get; set; }
        public int TotalCoupons { get; set; }
        public DateTime Issued { get; set; }

        public double Maturity { get => GetMaturityYears(); }
        public DateTime Matures { get => GetMaturityDate(); }
        public double Profitability { get => GetProfitability(true); }

        private double GetProfitability(bool currentProfitRate)
        {
            var maturity = GetMaturityDate();
            var prev = DateTime.Today;
            var prevValue = 0d;
            var lastDate = DateTime.Today;
            var lastValue = 0d;
            var disc = 1 + (currentProfitRate ? (CouponsInYear * CouponValue / FaceValue) : DiscountRate());
            var total = 0d;

            foreach (var income in GetIncomeStream())
            {
                prevValue = lastValue;
                total += income.Value * Math.Pow(disc, ((maturity - income.When).TotalDays) / DaysInYear);
            }
            return Math.Pow(total / Price, DaysInYear/ (maturity - lastDate).TotalDays) - 1;
        }

        private double GetMaturityYears()
        {
            if (CouponsInYear == 0)
                return double.PositiveInfinity;
            var years = TotalCoupons / CouponsInYear;
            var ostYears = TotalCoupons % CouponsInYear;
            var last = Issued.AddYears(years);
            last = last.AddDays(ostYears * 365 / CouponsInYear);
            return (last - DateTime.Now).TotalDays / 365;
        }

        private DateTime GetMaturityDate()
        {
            if (CouponsInYear == 0)
                return DateTime.MaxValue;
            var years = TotalCoupons / CouponsInYear;
            var ostYears = TotalCoupons % CouponsInYear;
            var last = Issued.AddYears(years);
            return last.AddDays(ostYears * DaysInYear / CouponsInYear);
        }

        public IEnumerable<FutureIncome> GetIncomeStream()
        {
            var now = DateTime.Today;
            var maturity = GetMaturityDate();
            if (maturity <= now || CouponsInYear == 0)
                yield break;

            var next = new DateTime(now.Year, Issued.Month, Issued.Day);
            if (next >= now)
                next = next.AddYears(-1);

            var yearsCounter = next.Year - Issued.Year;
            var inYearCounter = 1;
            var addDays = DaysInYear / CouponsInYear;
            while (next <= now)
            {
                if (inYearCounter % CouponsInYear == 0)
                {
                    yearsCounter++;
                    next = Issued.AddYears(yearsCounter);
                }
                else
                {
                    next = next.AddDays(addDays);
                }
                inYearCounter++;
            }

            while (next < maturity)
            {
                yield return new FutureIncome(CouponValue, next);

                if (inYearCounter % CouponsInYear == 0)
                {
                    yearsCounter++;
                    next = Issued.AddYears(yearsCounter);
                }
                else
                {
                    next = next.AddDays(addDays);
                }
                inYearCounter++;
            }

            yield return new FutureIncome(CouponValue, next);
            yield return new FutureIncome(FaceValue, next);
        }
    }
}
