﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FMI.Web.Data;
using MongoDB.Driver;

namespace FMI.Web.Services
{
    public class MongoBondsService : IBondsService
    {
        private string _connectionString;

        public MongoBondsService(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<Bond[]> GetBondsAsync()
        {
            var client = new MongoClient(_connectionString);
            var db = client.GetDatabase("FMI");

            var collection = db.GetCollection<Bond>("Calculator_Bonds");
            var res = await collection.FindAsync<Bond>(FilterDefinition<Bond>.Empty);
            var list = await res.ToListAsync<Bond>();
            return list.ToArray();
        }

        public async Task StoreBondAsync(Bond bond)
        {
            var client = new MongoClient(_connectionString);
            var db = client.GetDatabase("FMI");

            var collection = db.GetCollection<Bond>("Calculator_Bonds");
            if(bond.IsDeleted)
            {
                if (!bond.IsNew)
                {
                    await collection.DeleteOneAsync<Bond>(b => b._id == bond._id);
                }
            }
            else if (bond.IsNew)
            {
                await collection.InsertOneAsync(bond);
            }
            else if (bond.IsEdited)
            {
                await collection.ReplaceOneAsync<Bond>(b => b._id == bond._id, bond);
            }
        }
    }
}
