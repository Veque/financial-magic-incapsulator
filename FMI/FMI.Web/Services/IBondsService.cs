﻿using FMI.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMI.Web.Services
{
    public interface IBondsService
    {
        Task<Bond[]> GetBondsAsync();

        Task StoreBondAsync(Bond bond);
    }
}
