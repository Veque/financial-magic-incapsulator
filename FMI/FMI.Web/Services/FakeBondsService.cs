﻿using FMI.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMI.Web.Services
{
    public class FakeBondsService : IBondsService
    {
        private Dictionary<string, Bond> _bonds = new Dictionary<string, Bond>();

        public FakeBondsService()
        {
            _bonds = new Dictionary<string, Bond>
            {
                ["RU000A1019F9"] = new Bond
                {
                    Name = "RU000A1019F9",
                    FaceValue = 1000,
                    Price = 1149.21,
                    CouponValue = 40.64,
                    Issued = new DateTime(2012,2,12),
                    CouponsInYear = 2,
                    TotalCoupons = 30
                }
            };

        }


        public Task<Bond[]> GetBondsAsync()
        {
            return Task.FromResult(_bonds.Values.ToArray());
        }

        public Task StoreBondAsync(Bond bond)
        {
            _bonds[bond.Name] = bond;
            return Task.CompletedTask;
        }
    }
}
