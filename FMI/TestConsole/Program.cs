﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;


namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new int[] { -1 };
            Console.WriteLine(solution(a).ToString());
            Console.ReadKey();
        }

        public static int solution(int[] A)
        {
            var exists = new HashSet<int>(A.Length);
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] > 0)
                    exists.Add(A[i]);
            }

            for (int i = 1; i < A.Length + 2; i++)
            {
                if (!exists.Contains(i))
                {
                    return i;
                }
            }
            return 0;
        }
    }
}
