﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codility
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new int[] { 1, 4, 2, 2, 2, 2 };
            Console.WriteLine(solution(2, a).ToString());
            Console.ReadKey();
        }

        public static int solution(int X, int[] A)
        {
            var red = A.Length;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == X)
                {
                    red--;
                }
            }
            return red;
        }
    }
}
